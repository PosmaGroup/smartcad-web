'use strict';

// Define the `smarCad` module
angular.module('smartCad', [
  // ...which depends on the `smartCad` module
  'navbar',
  'ngRoute',
  'lprtable',
  'lprdetail',
  'home'

]);
