'use strict';

angular.
  module('smartCad').
  config(['$locationProvider' ,'$routeProvider',
    function config($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('!');

      $routeProvider.
        when('/smartCad', {
          template: '<home></home>'
        }).
        when('/smartCad/LPR', {
          template: '<lprtable></lprtable>'
        }).
        when('/smartCad/LPR/:id', {
          template: '<lprtable></lprtable>' + '<lprdetail></lprdetail>'
        }).
        otherwise('/smartCad');
    }
  ]);
