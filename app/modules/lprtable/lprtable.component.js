'use strict';

angular.
  module('lprtable').
  component('lprtable', {
    templateUrl: 'modules/lprtable/lprtable.template.html',
     controller: ['$http',
    function lprtableController($http) {
      var self = this;

      $http.get('data.json').then(function(response) {
        self.table = response.data;
      });
      }
      ]
  });
