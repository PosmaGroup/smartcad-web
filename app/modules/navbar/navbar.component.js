'use strict';

// Register `phoneList` component, along with its associated controller and template
angular.
  module('navbar').
  component('navbar', {
    templateUrl: 'modules/navbar/navbar.template.html',
    controller: function navbarController($scope) {
    }
  });